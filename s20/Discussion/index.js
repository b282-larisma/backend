// console.log("Hello World!");

// [Section] While Loop
// A while loop takes in an expression/condition
// Expressions are any unit of code that can be evaluated to a value
// If the condition evaluates to true, the statements inside the code block will be executed
// A statement is a command that the programmer gives to the computer
// A loop will iterate a certain number of times until an expression/condition is met
// "Iteration" is the term given to the repetition of statements

/*
SYNTAX:
	while(expression/condition) {
		statement
	}
*/

let count = 5;

// while the value of count is not equal to zero
while(count !== 0) {
	// the current value of count is printed out
	console.log("While: " + count);
	// decrease the value by 1 after every iteration to STOP the loop when it reaches 0
	count--;
};


// [SECTION] Do While Loop
// A do-while loop works a lot like while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once

/*
SYNTAX:
do {
	statement
} while (expression/condition)
*/

// let number = Number(prompt("Give me a number:"));

// do {
// 	// The current value of number is printed out
// 	console.log("Do While: " + number);
// 	// Increase the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater than 10
// 	number += 1;
// // Providing a number of 10 or greater will run the code block once and will stop the loop
// } while (number < 10);


// [SECTION] For Loop
/*
// A for loop is more flexible than while and do-while loops. It consists of three parts:
1. The "initialization" value that will track the progression of the loop.
2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
3. The "finalExpression" indicates how to advance the loop.
*/

/*
SYNTAX:
	for(initialization; expression/condition; finalExpression) {
		statement
	}
*/

for(let count = 0; count <=20; count++) {
	// the current value of count is printed out
	console.log(count);
};


// .length property
// Characters in string may be counted using the .length property
// The first character in a string corresponds to the number 0, the next is 1 up to the nth number
/*
a - index 0
l - index 1
e - index 2
x - index 3
*/
let myString = "alex";
console.log("Result of .length property: " + myString.length);

// Accessing elements of a string
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);


// Loop that will print out the individual letters of the myString variable
for(let i = 0; i < myString.length; i++) {
	// The current value of myString is printed out using its INDEX value
	console.log(myString[i]);
};


// Loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is a vowel

let myName = "AlEx";

for(let i=0;  i < myName.length; i++) {

	if (
		myName[i].toLowerCase() == "a" || 
		myName[i].toLowerCase() == "e" || 
		myName[i].toLowerCase() == "i" || 
		myName[i].toLowerCase() == "o" || 
		myName[i].toLowerCase() == "u" 
	) {
		// If the letter in the name is a vowel, it will print the number 3
		console.log(3);
	} else {
		// Print in the console all non-vowel characters in the name
		console.log(myName[i]);
	};
};


// [SECTION] Continue and Break Statements
// The "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
// The "break" statement is used to terminate the current loop once a match has been found

for(let count = 0; count <=20; count++) {
	// if remainder is equal to 0
	if(count % 2 === 0) {
		// Tells the code to continue to the next iteration of the loop
		// This ignores all statements located after the continue statement
		continue;
	};

	console.log("Continue and Break: " + count);

	if(count > 10) {
		// Tells the code to terminate/stope loop even if the expression/condition of the loop defines that should execute so long as the value of count is that or equal to 20
		// number values after 10 will no longer printed
		break;
	}
};

// Loop that will iterate based on the length of the string
// If the vowel is equal to a, continue to the next iteration of the loop
// If the current letter is equal to r, stop the loop

let name = "alexandro";

for(let i = 0; i < name.length; i++) {

	// If the vowel is equal to a, continue to the next iteration of the loop
	if(name[i].toLowerCase()=== "a") {
		console.log("Continue to the next iteration");
		continue;
	}
	// The current letter is printed out based on its index
	console.log(name[i]);

	// If the current letter is equal to "r", stop the loop
	if(name[i].toLowerCase() === "r") {
		break;
	};
};


