let http = require("http");

const app = http.createServer(function (request, response) {

   if (request.url == "/" && request.method == "GET") {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Welcome to booking system');
  }

  if (request.url == "/profile" && request.method == "GET") {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Welcome to your profile');
  }

  if (request.url == "/courses" && request.method == "GET") {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end("Here's our courses available");
  }

  if (request.url == "/addcourse" && request.method == "POST") {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Add course to our resources');
  }

  if (request.url == "/updatecourse" && request.method == "PUT") {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Update a course to our resources');
  }

  if (request.url == "/archivecourses" && request.method == "DELETE") {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Archive courses to our resources');
  }
})

//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;