// Contains instructions on HOW your API will perform its intended taks
// All the operations it can do will be placed in this file


// Uses the "require" directive to allow access to the "Task" model which allows us to access Mongoose methods to perform CRUD functions
// Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require("../models/task");

// Controller for getting all the tasks
// Defines the functions to be used in the "taskRoute.js" file and exports these functions
module.exports.getAllTasks = () => {
	// The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman
	return Task.find({}).then(result => {
		// The "return" statement returns the result of the MongoDB query to the "result" parameter defined in the "then" method
		return result;
	});
};


// Controller creating a task
// The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {

	// Creates a task object based on the Mongoose model "Task"
	let newTask = new Task({

		// Sets the "name" property with the value received from the client/Postman
		name: requestBody.name
	})
	// Saves the newly created "newTask" object in the MongoDB database
	// The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
	// The "then" method will accept the following 2 arguments:
		// The first parameter will store the result returned by the Mongoose "save" method
		// The second parameter will store the "error" object
	return newTask.save().then((task, error) => {
		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(error){
			console.log(error)
			// If an error is encountered, the "return" statement will prevent any other line or code below it and within the same code block from executing
			// Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
			// The else statement will no longer be evaluated
			return false;

		// Save successful, returns the new task object back to the client/Postman
		} else {
			return task
		};
	});
};


// Controller deleting a task
// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
module.exports.deleteTask = (taskId) => {
	// The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB
	// The Mongoose method "findByIdAndRemove" method looks for the document using the "_id" field
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err){
			console.log(err);
			return false;

		// Delete successful, returns the removed task object back to the client/Postman
		} else {
			return "Deleted task."
		};
	});
};


// Controller updating a task
// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
// The updates to be applied to the document retrieved from the "req.body" property coming from the client is renamed as "newContent"
module.exports.updateTask = (taskId, newContent) => {
	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	// "findById" is the same as "find({"_id" : value})"
	// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function 
	return Task.findById(taskId).then((result, err) => {
		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err){
			console.log(err);
			return false;
		}

		// Results of the "findById" method will be stored in the "result" parameter
		// It's "name" property will be reassigned the value of the "name" received from the request
		result.name = newContent.name;

		// Saves the updated object in the MongoDB database
		// The document already exists in the database and was stored in the "result" parameter
		// Because of Mongoose we have access to the "save" method to update the existing document with the changes we applied
		// The "return" statement returns the result of the "save" method to the "then" method chained to the "findById" method which invokes this function
		return result.save().then((updatedTask, saveErr) => {
			// If an error is encountered returns a "false" boolean back to the client/Postman
			if(saveErr){
				// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
				console.log(saveErr); 
				return false;
			// Update successful, returns the updated task object back to the client/Postman
			} else {
				// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
				return "Task updated.";
			};
		});
	});
};



// ################
// ACTIVITY SECTION

// Controller for getting a specific task

module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result
	})
}

// Controller for changing the status to complete
module.exports.changeStatusToComplete = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		} 

		result.status = "complete"
		return result.save().then((updateTask,saveErr) => {
			if(saveErr){
				console.log(saveErr)
				return false
			} else {
				return "Task status updated to complete."
			}
		})
		
	})
}