// 1. What directive is used by Node.js in loading the modules it needs?

require() function
    
let myModule = require("myModule");

console.log(myModule.message);

// 2. What Node.js module contains a method for server creation?

contains a method for server creation is called "http"

let http = require("http");

let server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "text/plain");
  res.end("Hello, World!");
});

server.listen(3000, "localhost", () => {
  console.log("Server running at http://localhost:3000/");
});

// 3. What is the method of the http object responsible for creating a server using Node.js?

The `createServer()` method is used to create a new server instance

const http = require("http");

const server = http.createServer((req, res) => {
  res.writeHead(200, { "Content-Type": "text/plain" });
  res.end("Hello World!");
});

server.listen(8080);

// 4. What method of the response object allows us to set status codes and content types?

response.setHeader("Content-Type", "text/html");
response.setHeader("Status", "200");

// 5. Where will console.log() output its contents when run in Node.js?

In the console or terminal window.

// 6. What property of the request object contains the address's endpoint?

`req.url` property provides the URL path portion of the incoming request, including the endpoint or route requested by the client.

const server = http.createServer((req, res) => {
  console.log('Request URL:', req.url);
  res.end('Hello, World!');
});