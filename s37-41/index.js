const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")

const userRoute = require("./routes/userRoute")

const courseRoute = require("./routes/courseRoute")

const app = express();

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://benedictlarisma2001:jpjEXz5875h5hm41@wdc028-course-booking.bz7joyt.mongodb.net/courseBookingAPI",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connecting to MongoDB locally
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors())

app.use("/users", userRoute)
app.use("/courses", courseRoute)

// "process.env.PORT" is an environment variable that typically holds the port number on which the server should listen.
app.listen(process.env.PORT || 4000, () => console.log(`Now listening to port ${process.env.PORT || 4000}!`));