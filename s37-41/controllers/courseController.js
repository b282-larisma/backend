/*
Controllers for creating a new course
1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
2. Save the new Course to the database
*/
const Course = require ("../models/Course");
const auth = require("../auth");

module.exports.addCourse = (reqBody, checkIfAdmin) => {
    if (checkIfAdmin !== true) {
        return false;
    }

    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });

    return newCourse.save().then((course, error) => {
        if (error) {
            return false;
        } else {
            return true;
        };
    });
};

// Controllers for retrieving all the courses
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result
    })
}

// Controllers for retrieving all active courses
module.exports.getAllActive = () => {
    return Course.find({isActive: true}).then(result => {
        return result
    })
}

// Controllers for retrieving a specific course
module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result;
    });
};

// Controllers for updating a course
module.exports.updateCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
        if (error) {
            return false;
        } else {
            return true;
        };
    });
};

// Controllers for deleting a course
module.exports.deleteCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then((result, err) => {
        if(err){
            return false
        } 

        result.isActive = false

        return result.save().then((deleteCourse, saveErr) => {
            if(saveErr){
                console.log(saveErr)
                return false
            } else {
                return true
            }
        })
        
    })
}