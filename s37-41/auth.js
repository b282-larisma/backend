// const jwt = require("jsonwebtoken")

// // User defined string data that will be used to create our json web tokens

// const secret = "CourseBookingAPI"

// // [SECTION JSON Web Token]

// // Token Creation
// module.exports.createAcessToken = (user) => {
// 	const data = {
// 		id: user._id,
// 		email: user.email,
// 		isAdmin: user.isAdmin
// 	}

// 	return jwt.sign(data, secret, {})
// }

const jwt = require("jsonwebtoken")

// User defined string data that will be used to create our JSON web tokens
const secret = "CourseBookingAPI"

// [SECTION JSON Web Token]

// Token Creation
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

// Token Verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	if(typeof token !== "undefined") {
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return res.send({auth: "failed"})
			} else {
				next()
			}
		})
	} else {
		return res.send({auth: "failed"})
	}
}

// Token Decryption
module.exports.decode = (token) => {
	if (typeof token !== "undefined") {
		console.log(token)
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null
			} else {
				return jwt.decode(token, {complete:true}).payload
			}
		})
	} else {
		return null
	}
}

// Token admin verification
module.exports.verifyAdmin = (req, res, next) => {
    let token = req.headers.authorization

    if (typeof token !== "undefined") {
        token = token.slice(7, token.length);

        jwt.verify(token, secret, (err, decoded) => {
            if (err || !decoded.isAdmin) {
                return res.send({ auth: "Unauthorized Access" });
            } else {
                next();
            }
        });
    } else {
        return res.send({ auth: "Unauthorized Access" });
    }
};
