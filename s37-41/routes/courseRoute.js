// Routes contains all the endpoints for our application
const express = require ("express");
const router = express.Router();
const courseController = require ( "../controllers/courseController");
const auth = require("../auth");


// route for Creating course
router.post("/create", auth.verifyAdmin, (req, res) => {
    const checkIfAdmin = auth.decode(req.headers.authorization).isAdmin;
    courseController.addCourse(req.body, checkIfAdmin).then(resultFromController => res.send(resultFromController));
});

// route for Creating courses
router.get("/all", (req, res) => {
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// route for retrieving active courses
router.get("/active", (req, res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// route for retrieving a specific course
router.get("/:courseId", (req, res) => {
    courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
    courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// route for deleting a course
router.patch("/:courseId", auth.verify, (req, res) => {
    courseController.deleteCourse(req.params).then(resultFromController => res.send(resultFromController));
})

module.exports = router