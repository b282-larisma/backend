/*
=====================
What is a data model?
=====================
A data model describes how data is organized or group in a database.
By creating data models, we anticipate which data will be managed by the database system and the application to be developed.

===================================
Relationships can have three types:
===================================
One-to-one
E.g. A person can only have one employee ID on a given company.
One-to-many
E.g. A person can have one or more email addresses.
Many-to-many
E.g. A book can be written by multiple authors and an author can write multiple books.

===========================
Entity-relationship diagram
===========================
An entity-relationship diagram (or ERD) is a diagram used to describe the structure of the database design.
It aims to show what attributes (fields) an entity (collection) has. It also shows the relationship of attributes between entities.
*/
