// CRUD Operations

// Create Operation
// insertOne() - Inserts one document to the collection
db.users.insertOne({
	"firstName": "John",
	"lastName": "Smith"
})

// insertMany() - Inserts multiple documents to the collection
db.users.insertMany([
	{ "firstName": "John", "lastName": "Doe" },
	{ "firstName": "Jane", "lastName": "Doe" }
])

// Read Operation
// find() - get all the inserted users
db.users.find()

// Retrieving specific documents
db.users.find({ "lastName": "Doe"})

// Update Operation
// updateOne() -  modify one document
db.users.updateOne(
	{
		"_id": ObjectId("648afd83de75a296077d20e2")
	},
	{
		$set: {
			"email": "johnsmith@gmail.com"
		}
	}
)

// updateMany() - modify multiple documents
db.users.updateMany(
	{
		"lastName": "Doe"
	},
	{
		$set: {
			"isAdmin": false
		}
	}
)

//we can also replace the whole document
db.users.replaceOne(
	{firstName: "Bill"},
  {
  	firstName: "Bill",
    lastName: "Gates",
    age: 65,
    contact: {
    	phone: "12345678",
      email: "bill@gmail.com"
    },
    courses: ["PHP","Laravel","HTML"],
    department:"Operations"
  }
)

// Delete Operation
// deleteMany - deletes multiple documents
db.users.deleteMany({ "lastName": "Doe" })

// deleteOne - deletes singe document
db.users.deleteOne({ "_id": ObjectId("648afd83de75a296077d20e2") })

//Advanced Queries

// Query an embedded document
db.users.find({
    contact: {
        phone: "87654321",
        email: "stephenhawking@gmail.com"
    }
});

// Query on nested field
db.users.findOne(
    {"contact.email": "janedoe@gmail.com"}
);

// Querying an Array with Exact Elements
db.users.find( { courses: [ "CSS", "Javascript", "Python" ] } );

// Querying an Array without regard to order
db.users.find( { courses: { $all: ["React", "Python"] } } );

// Querying an Embedded Array

db.users.insertOne({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});

db.users.find({
    namearr: 
        {
            namea: "juan"
        }
});
