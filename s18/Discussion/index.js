// console.log("Hello World!");

// Functions
// function printInput(){
// 	let nickname = prompt("Enter your nickname:");
// 	console.log("Hi, " + nickname);
// }
// printInput();

// Parameters and Arguments

// (name) - is called a parameter
// A "parameter" acts as a named variable/container that exists only inside a function

// let name = "Juana";

function printName(name){
	console.log("My name is " + name);
}

// ("Juana") - the information/data provided directly into the function is called an argument
printName("Juana");
printName("John");
printName("Jane");

// Variables can also be passed as an argument
let sampleVariable = "Yui";
printName(sampleVariable);


function checkDivisibilityBy8(num){

	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(65);
checkDivisibilityBy8(28);


// Functions as Arguments
function argumentFunction() {
	console.log("This function was passed as an argument before the message was printed.")
}

function invokeFunction(argumentFunction){
	argumentFunction();
}
invokeFunction(argumentFunction);
console.log(argumentFunction);

// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

function createFullName(firstName, middleName, lastName) {
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}
// "Juan" will be stored in the parameter "firstName"
// "Dela" will be stored in the parameter "middleName"
// "Cruz" will be stored in the parameter "lastName"
createFullName('Juan', 'Dela', 'Cruz');
createFullName('Juan', 'Dela', 'Cruz', "Hello");
createFullName('Juan', 'Dela');

// Using variable as arguments
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);


function createFullName(middleName, firstName, lastName) {
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}
// "Dela" will be stored in the parameter "firstName"
// "Juan" will be stored in the parameter "middleName"
// "Cruz" will be stored in the parameter "lastName"
createFullName('Juan', 'Dela', 'Cruz');


