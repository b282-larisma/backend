// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added

let trainer = {
    name: "Ash Ketchum",
    age: 10,
    Pokemon:["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn:["May", "Max"],
        kanto:["Brock", "Misty"]
        },
    talk: function f(pokemon) {
        console.log(pokemon + "! I choose you!")
    }
}

// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method

console.log(trainer)
console.log("Result of dot notation:")
console.log(trainer.name)
console.log("Result of square bracket notation:")
console.log(trainer["Pokemon"])
console.log("Result of talk method:")
trainer.talk("Pikachu")

// Create a constructor function called Pokemon for creating a pokemon

function Pokemon (name, level) {
    this.name = name;
    this.level = level
    this.health = 2 * level
    this.attack = level

    this.tackle = function f(target){
        console.log(this.name + ' tackled ' + target.name)
        console.log("targetPokemon's health is now reduced to "+ Number(target.health - this.attack))
        if (Number(target.health - this.attack) <= 0) {
            target.faint()
        }
    }
    this.faint = function f() {
        console.log(this.name + ' fainted.')
    }
}

// Create/instantiate a new pokemon

let pickachu = new Pokemon('Pickachu', 12)
console.log(pickachu)

// Create/instantiate a new pokemon

let geodude = new Pokemon('Geodude', 8)
console.log(geodude)

// Create/instantiate a new pokemon

let mewtwo = new Pokemon('Mewtwo', 100)
console.log(mewtwo)

// Invoke the tackle method and target a different object

geodude.tackle(pickachu)
console.log(pickachu)

// Invoke the tackle method and target a different object

mewtwo.tackle(geodude)
console.log(geodude)

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}