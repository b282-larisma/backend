/*
    Create functions which can manipulate our arrays.
*/

/*
    Important note: Don't pass the arrays as an argument to the function. 
    The functions must be able to manipulate the current given arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

console.log("registeredUsers")
console.log(registeredUsers)
/*
    
   1. Create a function called register which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, return the message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and return the message:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
    
function register(user) {
    let checkUser = registeredUsers.includes(user)
    if (checkUser == true) {
        console.log("register(\"" + user + "\")")
        console.log("Registration failed. Username already exists!")
    } else if (checkUser == false) {
        console.log("register(\"" + user + "\")")
        registeredUsers.push(user)
        console.log("Thank you for registering!")
        
    }
}
register("James Jeffries")
register("Conan O' Brien")
register("Conan O' Brien")



/*
    2. Create a function called addFriend which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then return the message with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, return the message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

console.log("registeredUsers")
console.log(registeredUsers)

function addFriend(user) {
    let checkUser = registeredUsers.includes(user)
    if (checkUser == false) {
        console.log("addFriend(\"" + user + "\")")
        console.log("User not found.")
    } else if (checkUser == true) {
        console.log("addFriend(\"" + user + "\")")
        friendsList.push(user)
        console.log("You Have added " + user + " as a friend!")
    }
}

addFriend("Victor Magtanggol")
addFriend("Conan O' Brien")
console.log(friendsList)

/*
    3. Create a function called displayFriends which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty return the message: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    
function displayFriends() {
    let friends = registeredUsers.length
    if (friends == 0) {
        console.log("displayFriends()")
        console.log("You currently have 0 friends. Add one first.")
    }

    else if (friends > 0) {
        console.log("displayFriends()")
        console.log(friendsList.join(", "))
    }
}

displayFriends()

/*
    4. Create a function called displayNumberOfFriends which will display the amount of registered users in your friendsList.
        - If the friendsList is empty return the message:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty return the message:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

function displayNumberOfFriends() {
    let numberoOfFriends = friendsList.length

    if (numberoOfFriends == 0) {
        console.log("displayNumberOfFriends()")
        console.log("You currently have 0 friends. Add one first.")
    }
    else if (numberoOfFriends > 0) {
        console.log("displayNumberOfFriends()")
        console.log("You currently have " + numberoOfFriends + " friends.")
    }
}

displayNumberOfFriends()

/*
    5. Create a function called deleteFriend which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty return a message:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

function deleteFriend(user) {
    let numberoOfFriends = friendsList.length

    if (numberoOfFriends == 0) {
        console.log("deleteFriend()")
        console.log("You currently have 0 friends. Add one first.")
    }
    else {
        console.log("deleteFriend()")
        friendsList.pop()
    }

}

deleteFriend()
console.log(friendsList)
deleteFriend()

// deleteFriend()
// console.log(friendsList)

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

//For exporting to test.js
try{
    module.exports = {

        registeredUsers: typeof registeredUsers !== 'undefined' ? registeredUsers : null,
        friendsList: typeof friendsList !== 'undefined' ? friendsList : null,
        register: typeof register !== 'undefined' ? register : null,
        addFriend: typeof addFriend !== 'undefined' ? addFriend : null,
        displayFriends: typeof displayFriends !== 'undefined' ? displayFriends : null,
        displayNumberOfFriends: typeof displayNumberOfFriends !== 'undefined' ? displayNumberOfFriends : null,
        deleteFriend: typeof deleteFriend !== 'undefined' ? deleteFriend : null

    }
} catch(err){

}