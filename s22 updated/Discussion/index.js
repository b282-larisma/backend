//Array Methods
// JavaScript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

//Mutator Methods
// Mutators methods are functions that "mutate" or change an array after they are created
//These methods manipulate the original array performing various such as adding or removing elements.

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
console.log("Current array: ");
console.log(fruits); // (4) ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit']

// push()
// Adds an element in the end of an array and returns the updated array's length
/*
SYNTAX:
    arrayName.push();
*/
let fruitsLength = fruits.push('Mango');

console.log(fruitsLength); // 5
console.log('Mutated array from push method: ');
console.log(fruits); // (5)['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango']

// Pushing multple elements to an array
fruitsLength = fruits.push('Avocado', 'Guava');

console.log(fruitsLength); // 7
console.log("Mutated array after pushing multiple elements:");
console.log(fruits); // (7)['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado', 'Guava']


// pop()
// removes the last element AND returns the removed element
/*
SYNTAX:
    arrayName.pop();
*/

console.log("Current Array: ");
console.log(fruits); // (7)['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado', 'Guava']

let removedFruit = fruits.pop();

console.log("Removed fruit: ");
console.log(removedFruit); // Guava
console.log("Mutated Array from the pop method:");
console.log(fruits); // (6)['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']



//unshift()
// it adds one or more elements at the beginning of an array AND it returns the update array length
/*
SYNTAX:
    arrayName.unshift('elementA');
    arrayName.unshift('elementA', 'elementB ' . . .)
*/
console.log("Current Array:");
console.log(fruits); // (6)['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

fruitsLength = fruits.unshift('Lime', 'Banana');

console.log(fruitsLength); // 8
console.log("Mutated array from unshift method: ");
console.log(fruits); // (8)['Lime', 'Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']


//shift()
// removes an element at the beginning of an array AND returns the removed element.
/*
SYNTAX:
    arrayName.shift();
*/

console.log("Current Array: ");
console.log(fruits); // (8)['Lime', 'Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

removedFruit = fruits.shift();
console.log("Removed Fruit");
console.log(removedFruit); // Lime
console.log("Mutated array from the shift method: ");
console.log(fruits); // (7)['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

// splice()
// Simultaneously removes an elements from a specified index number and adds element
/*
SYNTAX:
    arrayname.splice(startingIndex, deleteCount, elementsToBeAdded)
*/
console.log("Current Array: ");
console.log(fruits); // (7)['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('Mutated array from splice method:');
console.log(fruits); // (7)['Banana', 'Lime', 'Cherry', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

//sort()
// Rearranges the array elements in alphanumeric order
/*
SYNTAX:
    arrayName.sort();
*/
console.log("Current Array: ");
console.log(fruits); // (7)['Banana', 'Lime', 'Cherry', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

fruits.sort();

console.log("Mutated array from the sort method: ");
console.log(fruits); // ['Avocado', 'Banana', 'Cherry', 'Dragon Fruit', 'Kiwi', 'Lime', 'Mango']


//reverse()
// reverses the order of array elements
/*
SYNTAX:
    arrayName.reverse();
*/
console.log("Current Array: ");
console.log(fruits); // (7)['Avocado', 'Banana', 'Cherry', 'Dragon Fruit', 'Kiwi', 'Lime', 'Mango']

fruits.reverse();

console.log("Mutated array from the reverse method: ");
console.log(fruits); // (7)['Mango', 'Lime', 'Kiwi', 'Dragon Fruit', 'Cherry', 'Banana', 'Avocado']

// [Section] Non-mutator methods
// Non-mutator methods are functions that do not modify or change an array after the're created
// These methods do not manipulate the original array performing various task such as returning elements from an array and combining arrays and printing the output.

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
console.log(countries); // (8)['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']


// indexOf()
// returns the index number of the first matching element found in an array.
// If no match was found, the result will be -1.
//The search process will be done from the first element proceeding to the last element
/*
SYNTAX:
    arrayName.indexOf(searchValue);
    arrayName.indexOf(searchValue, startingIndex);
*/

let firstIndex = countries.indexOf('PH');
console.log(firstIndex); // 1

let invalidCountry = countries.indexOf('BR');
console.log(invalidCountry); // -1

firstIndex = countries.indexOf('PH', 2);
console.log(firstIndex); // 5

console.log(countries); // (8)['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

// lastIndexOf()
// returns the index number of the last matching element found in an array
// the search process will be done from last element proceeding to the first element
/*
SYNTAX:
    arrayName.lastIndexOf(searchValue);
    arrayName.lasIndexOf(searchValue, startingIndex);
*/

let lastIndex = countries.lastIndexOf('PH');
console.log(lastIndex); // 5

invalidCountry = countries.lastIndexOf('BR');
console.log(invalidCountry); // -1

lastIndexOf = countries.lastIndexOf('PH',   6);
console.log(lastIndexOf); // 5

console.log(countries); // console.log(countries); // (8)['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']


// slice()
// portions/slices elements from an array AND retrun a new array
/*
SYNTAX:
    arrayName.slice(startingIndex);
    arrayName.slice(startingIndex, endingIndex);
*/

// Slicing off elements from a specified index to the last element.
let slicedArrayA = countries.slice(2);
console.log('Result from slice method:');
console.log(slicedArrayA); // (6)['CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

// Slicing off elements from a specified index to another index:
// The elements that will be sliced are elements from the starting index until the element before the ending index.
let slicedArrayB = countries.slice(2, 4);
console.log('Result from the slice method:');
console.log(slicedArrayB); // (2)['CAN', 'SG']

//slicing off elements starting from the last element of an array:
let slicedArrayC = countries.slice(-3);
console.log('Result from the slice method:');
console.log(slicedArrayC); // (3)['PH', 'FR', 'DE']

//toString()
// returns an array as string separated by commas
/*
SYNTAX:
    arrayName.toString();
*/

let stringArray = countries.toString();
console.log('Result from toString method:')
console.log(stringArray); // US,PH,CAN,SG,TH,PH,FR,DE

console.log(typeof stringArray); // string

/*
===========
Additional
===========
*/

// concat()
// combines arrays to an array or elements and returns the combined result.
/*
SYNTAX:
    arrayA.concat(arrayB);
    arrayA.concat(elementA);
*/
let tasksArrayA = ["drink HTML", "eat javascript"];
let tasksArrayB = ["inhale CSS", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method:');
console.log(tasks); // (4)['drink HTML', 'eat javascript', 'inhale CSS', 'breathe sass']

// Combining multiple arrays
console.log('Result from concat method:');
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks); // (6)['drink HTML', 'eat javascript', 'inhale CSS', 'breathe sass', 'get git', 'be node']

// Combining arrays with elements
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat method:');
console.log(combinedTasks); // ['drink HTML', 'eat javascript', 'smell express', 'throw react']

//join()
// returns an array as string separated by speciefied separator string
/*
    arrayName.join("separatorString")
*/
let users = ['John', 'Jane', 'Joe', 'Robert'];

console.log(users.join()); // John,Jane,Joe,Robert
console.log(users.join('')); // JohnJaneJoeRobert
console.log(users.join(" - ")); // John - Jane - Joe - Robert


// [Section] Iteration Methods
// Iteration methods are loops designed to perform repititive task
// Iteration methods loops over all items in an array


// forEach()
// Similar to a for loop that iterates on each of array element.

/*
SYNTAX:
    arrayName.forEach(function(indivElement) {
        statement
    })
*/

console.log(allTasks);
// ['drink HTML', 'eat javascript', 'get git', 'be node', 'inhale CSS', 'breathe sass']

allTasks.forEach(function(task) {
    console.log(task);
    // drink HTML
    // eat javascript
    // get git
    // be node
    // inhale CSS
    // breathe sass
});

// Using forEach with conditional statements
let filteredTasks = [];

// Looping through all Array Items
allTasks.forEach(function(task) {
    // If the element/string's length is greater than 10 characters
    if(task.length > 10) {
        // Add the element to the filteredTasks array
        filteredTasks.push(task);
    }
});
console.log("Result of filtered tasks:");
console.log(filteredTasks); // (2)['eat javascript', 'breathe sass']

//map()
// Iterates on each element and returns new array with diffrenet values depending on the result of the function's operation

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number) {
    return number * number;
});

console.log("Original Array:");
console.log(numbers);// (5)[1, 2, 3, 4, 5] - Original is unaffected by map()
console.log("Result of map method:");
console.log(numberMap);//(5)[1, 4, 9, 16, 25] - A new array is returned by map() and stored in the variable.

// filter()
// returns new array that contains elements which meets the given
//returns an empty array if no elements were found
/*
SYNTAX:
    let/const resultArray = arrayName.filter(function(indivElement){
        return expression/condtion;
    })
*/
let filterValid = numbers.filter(function(number) {
    return (number <  3); 
});
console.log("Result of filter method:");
console.log(filterValid); // (2)[1, 2]

// No elements found
let nothingFound = numbers.filter(function(number) {
    return (number = 0);
})
console.log("Result of filter method:");
console.log(nothingFound); // []


// includes()
// includes() method checks if the argument passed can be found in the array.
// it returns a boolean which can be saved in a variable.
// returns true if the argument is found in the array.
// returns false if it is not.
/*
SYNTAX:
    arrayName.includes(<argumentToFind>)
*/

let products = ["Mouse", 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes('Mouse');
console.log(productFound1); // true

/*
===========
Additional
===========
*/

// every()
// it will check if all elements in an array meet the given the condition
// return true value if all elements meet the condition and false otherwise
/*
SYNTAX:
    let/const resultArray = arrayName.every(function(indivElement) {
        return expression/condition;
    })
*/

numbers = [1, 2, 3, 4, 5];

let allValid = numbers.every(function(number){
    return (number<6);
}); 

console.log(allValid); // true

//some()
// checks if at least one element in the array meets the given condition.
/*
SYNTAX:
    let/const resultArray = arrayName.some(function(indivElement){
        return expression/condtion;
    })
*/

let someValid = numbers.some(function(number){
    return (number < 2);
})

console.log(someValid); // true

// reduce()
// evaluates elements from left to right and returns/reduces the array into single value
// The "accumulator" parameter in the function stores the result for every iteration of the loop
// The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
/*
SYNTAX:
    let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
        return expression/operation
    })
*/
// reducing string arrays
let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x, y) {
    return x + ' ' + y;
});
console.log("Result of reduce method: " + reducedJoin); // Result of reduce method: Hello Again World
