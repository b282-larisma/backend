const express = require("express");

// "mongoose" is a package that allows creation of schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();

const port = 3001;


// Connecting to MongoDB Atlas
// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
// Due to updates in Mongo DB drivers that allow connection to it, the default connection string is being flagged as an error
// By default a warning will be displayed in the terminal when the application is run, but this will not prevent Mongoose from being used in the application
// { newUrlParser : true } allows us to avoid any current and future errors while connecting to Mongo DB
mongoose.connect("mongodb+srv://benedictlarisma2001:jpjEXz5875h5hm41@wdc028-course-booking.bz7joyt.mongodb.net/s35",
	// allows us to avoid any current and future errors while connecting to MongoDB
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connecting to MongoDB locally
// Allows to handle errors when the initial connection is established
// Set notifications for connection success or failure
// Connection to the database
// Allows to handle errors when the initial connection is establised
// Works with the on and once Mongoose methods
let db = mongoose.connection;

// "console.error.bind" allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, "We're connected to the cloud database!" output in the console
db.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// [SECTION] Mongoose Schemas
// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
// The "new" keyword create a new Schema
// Use the Schema() constructore of the Mongoose module to create a new schema object
/*
SYNTAX:
	const schemaName = new mongoose.Schema({

})
*/
// const taskSchema = new mongoose.Schema({
// 	// Define the fields with the corresponding data type
// 	// For a task, it needs a "task name" and "task status"
// 	// There is a field called "name" and its data type is "String"
// 	name: String,
// 	status: {
// 		type: String,
// 		// Default values are the predefined values for a field if we don't put any value
// 		default: "pending"
// 	}
// });


// // [SECTION] Models
// // The variable/object "Task"can now used to run commands for interacting with our database
// // Uses schemas and are used to create/instantiate objects that correspond to the schema
// // Models must be in singular form and first letter is capitalized
// // First parameter of the mongoose model method indicates the collection in where to store the the data
// // Second parameter is used to used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
// /*
// SYNTAX:
// const Model = mongoose.model("collectionName", schemaName);
// */
// const Task = mongoose.model("Task", taskSchema);

// /*
// Creating a new task
// 1. Add a functionality to check if there are duplicate tasks
// 	- If the task already exists in the database, we return a message
// 	- If the task doesn't exist in the database, we add it in the database
// 2. The task data will be coming from the request's body
// 3. Create a new Task object with a "name" field/property
// 4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
// */


// app.post("/tasks", (req, res) => {
// 	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
// 	// findOne() returns the first document that matches the search criteria as a single object.
// 	Task.findOne({name: req.body.name}).then((result, err) => {
// 		// If a document was found and the document's name matches the information sent via the client/Postman
// 		if(result != null && result.name == req.body.name) {
// 			// Return a message to the client/Postman
// 			return res.send("Duplicate task found!");
// 		// If no document was found
// 		} else {
// 			// Create a new task and save it to the database
// 			let newTask = new Task({
// 				name: req.body.name
// 			});
// 			// "save()" method will store the information to the database
// 			// The "save" method will store the information to the database
// 			// Since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save to the database
// 			// The "save()" method can send the result or error in another JS method called then()
// 			// the .save() method returns the result first and the error second as parameters.
// 			newTask.save().then((savedTask, saveErr) => {
// 				// If there are errors in saving
// 				if(saveErr) {
// 					// Will print any errors found in the console
// 					return console.error(saveErr);
// 				// No error found while creating the document
// 				} else {
// 					return res.status(201).send("New Task created!");
// 				};
// 			}); 
// 		};
// 	});
// });


// /*
// Getting all the tasks
// 1. Retrieve all the documents
// 2. If an error is encountered, print the error
// 3. If no errors are found, send a success status back to the client/Postman and return an array of documents
// */

// app.get("/tasks", (req, res) => {
// 	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
// 	Task.find({}).then((result, err) => {
// 		// If an error occurred
// 		if(err) {
// 			// Will print any errors found in the console
// 			return console.log(err);
// 		} else {
// 			// Status "200" means that everything is "OK" in terms of processing
// 			// The "json" method allows to send a JSON format for the response
// 			// The returned response is purposefully returned as an object with the "data" property to mirror real world complex data structures
// 			return res.status(200).json({
// 				data: result
// 			});
// 		};
// 	});
// });

// ACTIVITY

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model("User", userSchema)

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}).then((result, err) =>{
		if(result != null && result.username == req.body.username) {
			return res.send("Duplicate username found!")
		} else {
			if (req.body.username != "" && req.body.password != "") {
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				})

				newUser.save().then((savedTask, saveErr) => {
					if(saveErr) {
						return console.error(saveErr)
					} else {
						return res.status(201).send("New User registered!")
					}
				})
			} else {
				return res.send("BOTH username and password must be provided.")
			}
		}
	})
})

app.listen(port, () => console.log(`Server running at port ${port}!`));
