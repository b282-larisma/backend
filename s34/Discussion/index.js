// Use the "require" directive to load the express module/package
// A "module" is a software component or part of a program that contains one or more routines
// This is used to get the contents of the express package to be used by our application
// It also allows us access to methods and functions that will allow us to easily create a server
const express = require("express");

// Create an application using express
// This creates an express application and stores this in a constant called app
// In layman's terms, app is our server
const app = express();

// For our application server to run, we need a port to listen to
const port = 3000;

// Methods used from express.js are middleware
// Middleware is software that provides common services and capabilities to applications outside of what’s offered by the operating system
// API management is one of the common application of middlewares.

// Allows your app to read json data
app.use(express.json());

// Allows your app to read data from forms
// By default, information received from the url can only be received as a string or an array
// By applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended: true}));

// [SECTION] Routes
// Express has methods corresponding to each HTTP method
// This route expects to receive a GET request at the base URI "/"
// The full base URI for our local application for this route will be at "http://localhost:3000"
// This route will return a simple message back to the client

// GET
// This route expects to receive a GET request at the 
app.get("/greet", (request, response) => {
	// "response.send" method to send a response back to the client
	response.send("Hello from the /greet endpoint!")
});

// POST
// This route expects to receive a POST request at the URI "/hello"
app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});


// Simple Registration

// An array that will store user objects when the "/signup" route is accessed
// This will serve as our mock database
let users = [];

// This route expects to receive a POST request at the URI "/signup"
// This will create a user object in the "users" variable that mirrors a real world registration process
app.post("/signup", (request, response) => {
	// If contents of the "request body" with the property "username" and "password" is not empty
	if(request.body.username !== "" && request.body.password !== ""){
		// This will store the user object sent via Postman to the users array created above
		users.push(request.body);

		// This will send a response back to the client/Postman after the request has been processed
		response.send(`User ${request.body.username} successfully registered!`);

	// If the username and password are not complete an error message will be sent back to the client/Postman
	} else {
		response.send("Please input BOTH username and password!")
	}
});


// [SECTION] Activity

app.get("/home", (request, response) => {
	response.send(`Welcome to homepage!`)
})

app.get("/users", (request, response) => {
	response.send(users)
})

app.delete("/delete-user", (request, response) => {
	let userNameToDelete = request.body.username
	let message = ""
	let checkUser = users.includes(userNameToDelete)


	if(users.length > 0){

		for(let i=0;  i < users.length; i++) {
			
			if (checkUser) {
				users.splice(i, 1)
				message = `User ${userNameToDelete} has been deleted`
				userFound = true
				break
			} 
		}
		
		if(!checkUser){
			message = `User ${userNameToDelete} does not exist.`
			} 

	} else {
		message = "No users found"
	}

	response.send(message)
});

app.listen(port, () => console.log(`Server running at port ${port}`));

