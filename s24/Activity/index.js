//Important Note: Do not change the variable names. 
//All required classes, variables and function names are listed in the exports.

// Exponent Operator
let x = 2
const getCube = x ** 3

// Template Literals

answer = `The cube of ${x} is ${getCube}.`
console.log(answer)

// Array Destructuring
const address = ["258", " Washington Ave NW", " California", " 90011"]
const [houseNumber, street, state, zipCode] = address
myAddress = `I live at ${address}.`
console.log(myAddress)

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
const {name, species, weight, measurement} = animal
theAnimal = `${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`
console.log(theAnimal)

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
    console.log(number);
})

let reduceNumber =  numbers.reduce(
  (accumulator, currentValue) => accumulator + currentValue
);
console.log(reduceNumber)

class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const myDog = new Dog("Frankie", "5", "Miniature Dachshund");
console.log(myDog);

// Javascript Classes

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getCube: typeof getCube !== 'undefined' ? getCube : null,
        houseNumber: typeof houseNumber !== 'undefined' ? houseNumber : null,
        street: typeof street !== 'undefined' ? street : null,
        state: typeof state !== 'undefined' ? state : null,
        zipCode: typeof zipCode !== 'undefined' ? zipCode : null,
        name: typeof name !== 'undefined' ? name : null,
        species: typeof species !== 'undefined' ? species : null,
        weight: typeof weight !== 'undefined' ? weight : null,
        measurement: typeof measurement !== 'undefined' ? measurement : null,
        reduceNumber: typeof reduceNumber !== 'undefined' ? reduceNumber : null,
        Dog: typeof Dog !== 'undefined' ? Dog : null

    }
} catch(err){

}